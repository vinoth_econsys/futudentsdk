//
//  FutudentCamSDK.h
//  FutudentSDK
//
//  Created by Appsteam on 5/1/2014.
//  Copyright © 2017 econsys. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef struct hid_device_ device_handle; /**< opaque hidapi structure */

typedef struct FW_Version_Info {
    
    int MajorVersion;
    int MinorVersion;
    int SDKVersion;
    int SVNVersion;
}firmwareInfo;

typedef enum Device_State
{
    Arrival = 0,
    Removal = 1,
    NoState=2
    
}deviceState;

typedef enum
{
    SUCCESS = 0,
    FAILURE = -1,
    ERROR_INVALID_HANDLE = -2,
    ERROR_OUTOFRANGE = -3,
    ERROR_PARAMS_NULL = -4,
    ERROR_NOT_INIT = -5
    
    
}errorCode;

typedef enum
{
    NORMAL = 0X01,
    BLACKANDWHITE = 0X04,
    GRAYSCALE = 0X07,
    NEGATIVE = 0X08,
    SKETCH = 0X10
}special_Effects;

/*!
 @protocol deviceStatusProtocol
 @abstract
 Defines an interface for delegates of FutudentSDK to be
 notified of the device state whenever the state changes.
 */
@protocol deviceStatusProtocol <NSObject>

-(void)DeviceStatusCallBackDelegate:(deviceState)devicestate;

@end

@interface FutudentCamSDK : NSObject

/** @property deviceDelegate.
 @abstract
 Stores the deviceStatusProtocol passed as a parameter of Reg_DeviceStatus_CB API.
 */
@property (weak) id <deviceStatusProtocol> deviceDelegate;

/** @brief Registers the callback to get the device state change notifications.
 @ingroup FutudentSDKAPIs
 @param handle A device handle to communicate with.
 @param delegate A delegate instance to which the notifications will be sent.
 @returns
 This function returns
 - ERROR_NOT_INIT       If the device not initialized
 - ERROR_INVALID_HANDLE If the handle passed is invalid
 - FAILURE              If the delegate instance is NULL
 - SUCCESS              on success
 */
-(int) Reg_DeviceStatus_CB:(device_handle*)handle CBdelegate:(id<deviceStatusProtocol>)delegate;

/** @brief Checks whether any FUTUDENT device is connected.
 @ingroup FutudentSDKAPIs
 @param deviceList A NSMutableArray instance to which the uniqueId of the connected device will be Added.
 @returns
 This function returns -1 if no device is connected and 0 if atleast one device is connected
 */
- (int) Is_Device_Connected:(NSMutableArray*)deviceList;

/** @brief Initializes the FUTUDENT device.
 @ingroup FutudentSDKAPIs
 @param deviceId The uniqueId of the connected device.
 @returns
 This function returns a handle to the device to communicate with and NULL on error.
 */
- (device_handle*) initialize_Device:(NSString*)deviceId;

/** @brief Deinitializes the device
 @ingroup FutudentSDKAPIs
 @param handle Address of the device handle to communicate with.
 @returns
 This function returns
 - ERROR_NOT_INIT       If the device not initialized
 - ERROR_INVALID_HANDLE If the handle passed is invalid
 - FAILURE              If any error in HID layer
 - SUCCESS              on success
 */
-(int) Deinitalize_Device:(device_handle**)handle;

/** @brief Gets the device's Firmware Version.
 @ingroup FutudentSDKAPIs
 @param handle A device handle to communicate with.
 @param versionInfo A firmwareInfo instance to which the Firmware Version will be written.
 @returns
 This function returns
 - ERROR_NOT_INIT       If the device not initialized
 - ERROR_INVALID_HANDLE If the handle passed is invalid
 - ERROR_PARAMS_NULL    If the string instance is NULL
 - FAILURE              If any error in HID layer
 - SUCCESS              on success
 */
-(int) get_FirmwareVersion:(device_handle*)handle version_Info:(firmwareInfo*) versionInfo;

/** @brief Gets the device's UniqueId.
 @ingroup FutudentSDKAPIs
 @param handle A device handle to communicate with.
 @param uniqueId A NSString instance to which the UniqueId will be written.
 @returns
 This function returns
 - ERROR_NOT_INIT       If the device not initialized
 - ERROR_INVALID_HANDLE If the handle passed is invalid
 - ERROR_PARAMS_NULL    If the string instance is NULL
 - FAILURE              If any error in HID layer
 - SUCCESS              on success
 */

-(int) Get_UniqueID:(device_handle *)handle strForUniqueid:(NSString**)uniqueId;

/** @brief Gets the device's Product String.
 @ingroup FutudentSDKAPIs
 @param handle A device handle to communicate with.
 @param productString A NSString instance to which the product string will be written.
 @returns
 This function returns
 - ERROR_NOT_INIT       If the device not initialized
 - ERROR_INVALID_HANDLE If the handle passed is invalid
 - ERROR_PARAMS_NULL    If the string instance is NULL
 - FAILURE              If any error in HID layer
 - SUCCESS              on success
 */
-(int) Get_ProductString:(device_handle *)handle strForProductString:(NSString**)productString;

/** @brief Sets the given string as device's Product String.
 @ingroup FutudentSDKAPIs
 @param handle A device handle to communicate with.
 @param productString A String which has to be written as product string.
 @returns
 This function returns
 - ERROR_NOT_INIT       If the device not initialized
 - ERROR_INVALID_HANDLE If the handle passed is invalid
 - ERROR_PARAMS_NULL    If the string instance is NULL
 - ERROR_OUTOFRANGE     If the string length is greater than 50 characters
 - FAILURE              If any error in HID layer
 - SUCCESS              on success
 */
-(int) Set_ProductString:(device_handle *)handle strForProductString:(NSString*)productString;

/** @brief Sets the device in authenticated mode.
 @ingroup FutudentSDKAPIs
 @param handle A device handle to communicate with.
 @returns
 This function returns
 - ERROR_NOT_INIT       If the device not initialized
 - ERROR_INVALID_HANDLE If the handle passed is invalid
 - FAILURE              If any error in HID layer
 - SUCCESS              on success
 */
-(int) set_Authentication:(device_handle*)handle;


/** @brief Sets the device in de-authenticated mode.
 @ingroup FutudentSDKAPIs
 @param handle A device handle to communicate with.
 @returns
 This function returns
 - ERROR_NOT_INIT       If the device not initialized
 - ERROR_INVALID_HANDLE If the handle passed is invalid
 - FAILURE              If any error in HID layer
 - SUCCESS              on success
 */
-(int) set_DeAuthentication:(device_handle*)handle;

/** @brief Enable or Disable the device's authenticated mode.
 @ingroup FutudentSDKAPIs
 @param handle A device handle to communicate with.
 @param mode value to Enable or Disable. 0x01 - Enable, 0x00 - Disable
 @returns
 This function returns
 - ERROR_NOT_INIT       If the device not initialized
 - ERROR_INVALID_HANDLE If the handle passed is invalid
 - ERROR_OUTOFRANGE     If the value passed is neither 0x01 nor 0x00
 - FAILURE              If any error in HID layer
 - SUCCESS              on success
 */
-(int) set_Authentication_Status:(device_handle*)handle mode:(UInt8)enableOrDisable;


/** @brief Gets the device's Authenticated status
 @ingroup FutudentSDKAPIs
 @param handle A device handle to communicate with.
 @param mode An address to which the status will be written.
 @returns
 This function returns
 - ERROR_NOT_INIT       If the device not initialized
 - ERROR_INVALID_HANDLE If the handle passed is invalid
 - ERROR_PARAMS_NULL    If the parameter passed is null
 - FAILURE              If any error in HID layer
 - SUCCESS              on success
 */
-(int) get_Authentication_Status:(device_handle*)handle mode:(UInt8*)enableOrDisable;

/** @brief Gets the camera's current special effect.
 @ingroup FutudentSDKAPIs
 @param handle A device handle to communicate with.
 @param mode An address to which the current effect will be written.
 @returns
 This function returns
 - ERROR_NOT_INIT       If the device not initialized
 - ERROR_INVALID_HANDLE If the handle passed is invalid
 - ERROR_PARAMS_NULL    If the parameter passed is null
 - FAILURE              If any error in HID layer
 - SUCCESS              on success

 */
-(int) get_Special_Effects:(device_handle*)handle mode:(UInt8*)effectMode;


/** @brief Sets the given special effect to the camera.
 @ingroup FutudentSDKAPIs
 @param handle A device handle to communicate with.
 @param mode Special effect mode to set. 
 @returns
 This function returns
 - ERROR_NOT_INIT       If the device not initialized
 - ERROR_INVALID_HANDLE If the handle passed is invalid
 - FAILURE              If any error in HID layer
 - SUCCESS              on success
 */
-(int) set_Special_Effects:(device_handle*)handle mode:(special_Effects)effectMode;


/** @brief Gets the camera's current denoise value.
 @ingroup FutudentSDKAPIs
 @param handle A device handle to communicate with.
 @param value An address to which the current denoise value will be written.
 @returns
 This function returns
 - ERROR_NOT_INIT       If the device not initialized
 - ERROR_INVALID_HANDLE If the handle passed is invalid
 - ERROR_PARAMS_NULL    If the parameter passed is null
 - FAILURE              If any error in HID layer
 - SUCCESS              on success
 */
-(int) get_denoise_value:(device_handle*)handle value:(UInt8*)denoiseValue;


/** @brief Sets the given denoise value to the camera.
 @ingroup FutudentSDKAPIs
 @param handle A device handle to communicate with.
 @param value denoise value to set. 
 @returns
 This function returns
 - ERROR_NOT_INIT       If the device not initialized
 - ERROR_INVALID_HANDLE If the handle passed is invalid
 - ERROR_OUTOFRANGE     If the mode passed is not in between 0x00 & 0x0F 
 - FAILURE              If any error in HID layer
 - SUCCESS              on success
 */
-(int) set_denoise_value:(device_handle*)handle value:(UInt8)denoiseValue;

/** @brief Gets whether the camera's flip mode is enabled or not.
 @ingroup FutudentSDKAPIs
 @param handle A device handle to communicate with.
 @param mode An address to which the current flip mode will be written.
 @returns
 This function returns
 - ERROR_NOT_INIT       If the device not initialized
 - ERROR_INVALID_HANDLE If the handle passed is invalid
 - ERROR_PARAMS_NULL    If the parameter passed is null
 - FAILURE              If any error in HID layer
 - SUCCESS              on success
 */
-(int) get_flip_mode:(device_handle*)handle mode:(UInt8*)flipMode;


/** @brief Sets the flip mode of the camera to enabled or disabled state.
 @ingroup FutudentSDKAPIs
 @param handle A device handle to communicate with.
 @param mode Flip mode to set. 
 @returns
 This function returns
 - ERROR_NOT_INIT       If the device not initialized
 - ERROR_INVALID_HANDLE If the handle passed is invalid
 - ERROR_OUTOFRANGE     If the value passed is neither 0x01 nor 0x00
 - FAILURE              If any error in HID layer
 - SUCCESS              on success
 */
-(int) set_flip_mode:(device_handle*)handle mode:(UInt8)flipMode;

/** @brief Gets whether the camera's mirror mode is enabled or not.
 @ingroup FutudentSDKAPIs
 @param handle A device handle to communicate with.
 @param mode An address to which the current mirror mode will be written.
 @returns
 This function returns
 - ERROR_NOT_INIT       If the device not initialized
 - ERROR_INVALID_HANDLE If the handle passed is invalid
 - ERROR_PARAMS_NULL    If the parameter passed is null
 - FAILURE              If any error in HID layer
 - SUCCESS              on success
 */
-(int) get_mirror_mode:(device_handle*)handle mode:(UInt8*)mirrorMode;


/** @brief Sets the mirror mode of the camera to enabled or disabled state.
 @ingroup FutudentSDKAPIs
 @param handle A device handle to communicate with.
 @param mode Mirror mode to set. 
 @returns
 This function returns
 - ERROR_NOT_INIT       If the device not initialized
 - ERROR_INVALID_HANDLE If the handle passed is invalid
 - ERROR_OUTOFRANGE     If the value passed is neither 0x01 nor 0x00
 - FAILURE              If any error in HID layer
 - SUCCESS              on success
 */
-(int) set_mirror_mode:(device_handle*)handle mode:(UInt8)mirrorMode;


/** @brief Gets whether the camera's autoFlicker mode is enabled or not.
 @ingroup FutudentSDKAPIs
 @param handle A device handle to communicate with.
 @param mode An address to which the current flicker mode will be written.
 @returns
 This function returns
 - ERROR_NOT_INIT       If the device not initialized
 - ERROR_INVALID_HANDLE If the handle passed is invalid
 - ERROR_PARAMS_NULL    If the parameter passed is null
 - FAILURE              If any error in HID layer
 - SUCCESS              on success
 */
-(int) get_autoFlicker_mode:(device_handle*)handle mode:(UInt8*)flickerMode;


/** @brief Sets the autoFlicker mode of the camera to enabled or disabled state.
 @ingroup FutudentSDKAPIs
 @param handle A device handle to communicate with.
 @param mode AutoFlicker mode to set. 
 @returns
 This function returns
 - ERROR_NOT_INIT       If the device not initialized
 - ERROR_INVALID_HANDLE If the handle passed is invalid
 - ERROR_OUTOFRANGE     If the value passed is neither 0x01 nor 0x00
 - FAILURE              If any error in HID layer
 - SUCCESS              on success
 */
-(int) set_autoFlicker_mode:(device_handle*)handle mode:(UInt8)flickerMode;


@end

