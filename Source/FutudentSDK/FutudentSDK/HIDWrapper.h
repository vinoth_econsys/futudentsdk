
/** @file
 * @defgroup API hidapi API
 */

#ifndef HIDAPI_H__
#define HIDAPI_H__

#include <wchar.h>
//#include "SDKHeaders.h"

#ifdef _WIN32
#define HID_API_EXPORT __declspec(dllexport)
#define HID_API_CALL
#else
#define HID_API_EXPORT /**< API export macro */
#define HID_API_CALL /**< API call macro */
#endif

#define HID_API_EXPORT_CALL HID_API_EXPORT HID_API_CALL /**< API export and call macro*/

#ifdef __cplusplus
extern "C" {
#endif
    struct hid_device_;
    typedef struct hid_device_ device_handle; /**< opaque hidapi structure */
    
    /** hidapi info structure */
    struct hid_device_info {
        /** Platform-specific device path */
        char *path;
        /** Device Vendor ID */
        unsigned short vendor_id;
        /** Device Product ID */
        unsigned short product_id;
        /** Serial Number */
        wchar_t *serial_number;
        /** Device Release Number in binary-coded decimal,
         also known as Device Version Number */
        unsigned short release_number;
        /** Manufacturer String */
        wchar_t *manufacturer_string;
        /** Product string */
        wchar_t *product_string;
        /** Usage Page for this Device/Interface
         (Windows/Mac only). */
        unsigned short usage_page;
        /** Usage for this Device/Interface
         (Windows/Mac only).*/
        unsigned short usage;
        /** The USB interface which this logical device
         represents. Valid on both Linux implementations
         in all cases, and valid on the Windows implementation
         only if the device contains more than one interface. */
        int interface_number;
        
        /** Pointer to the next device */
        struct hid_device_info *next;
    };
    
    
    /** @brief Initialize the HIDAPI library.
     This function initializes the HIDAPI library. Calling it is not
     strictly necessary, as it will be called automatically by
     hid_enumerate() and any of the hid_open_*() functions if it is
     needed.  This function should be called at the beginning of
     execution however, if there is a chance of HIDAPI handles
     being opened by different threads simultaneously.
     
     @ingroup API
     @returns
     This function returns 0 on success and -1 on error.
     */
    int HID_API_EXPORT HID_API_CALL hid_init(void);
    
    /** @brief Finalize the HIDAPI library.
     This function frees all of the static data associated with
     HIDAPI. It should be called at the end of execution to avoid
     memory leaks.
     @ingroup API
     @returns
     This function returns 0 on success and -1 on error.
     */
    int HID_API_EXPORT HID_API_CALL hid_exit(void);
    
    /** @brief Enumerate the HID Devices.
     This function returns a linked list of all the HID devices
     attached to the system which match vendor_id and product_id.
     If @p vendor_id is set to 0 then any vendor matches.
     If @p product_id is set to 0 then any product matches.
     If @p vendor_id and @p product_id are both set to 0, then
     all HID devices will be returned.
     @ingroup API
     @param vendor_id The Vendor ID (VID) of the types of device
     to open.
     @param product_id The Product ID (PID) of the types of
     device to open.
     @returns
     This function returns a pointer to a linked list of type
     struct #hid_device, containing information about the HID devices
     attached to the system, or NULL in the case of failure. Free
     this linked list by calling hid_free_enumeration().
     */
    struct hid_device_info HID_API_EXPORT * HID_API_CALL hid_enumerate(unsigned short vendor_id, unsigned short product_id);
    
    /** @brief Free an enumeration Linked List
     This function frees a linked list created by hid_enumerate().
     @ingroup API
     @param devs Pointer to a list of struct_device returned from
     hid_enumerate().
     */
    void  HID_API_EXPORT HID_API_CALL hid_free_enumeration(struct hid_device_info *devs);
    
    /** @brief Open a HID device using a Vendor ID (VID), Product ID
     (PID) and optionally a serial number.
     If @p serial_number is NULL, the first device with the
     specified VID and PID is opened.
     @ingroup API
     @param vendor_id The Vendor ID (VID) of the device to open.
     @param product_id The Product ID (PID) of the device to open.
     @param serial_number The Serial Number of the device to open
     (Optionally NULL).
     @returns
     This function returns a pointer to a #hid_device object on
     success or NULL on failure.
     */
    HID_API_EXPORT device_handle * HID_API_CALL hid_open(unsigned short vendor_id, unsigned short product_id, const wchar_t *serial_number);
    
    /** @brief Open a HID device by its path name.
     The path name be determined by calling hid_enumerate(), or a
     platform-specific path name can be used (eg: /dev/hidraw0 on
     Linux).
     @ingroup API
     @param path The path name of the device to open
     @returns
     This function returns a pointer to a #hid_device object on
     success or NULL on failure.
     */
    HID_API_EXPORT device_handle * HID_API_CALL hid_open_path(const char *path);
    
    /** @brief Write an Output report to a HID device.
     The first byte of @p data[] must contain the Report ID. For
     devices which only support a single report, this must be set
     to 0x0. The remaining bytes contain the report data. Since
     the Report ID is mandatory, calls to hid_write() will always
     contain one more byte than the report contains. For example,
     if a hid report is 16 bytes long, 17 bytes must be passed to
     hid_write(), the Report ID (or 0x0, for devices with a
     single report), followed by the report data (16 bytes). In
     this example, the length passed in would be 17.
     hid_write() will send the data on the first OUT endpoint, if
     one exists. If it does not, it will send the data through
     the Control Endpoint (Endpoint 0).
     @ingroup API
     @param device A device handle returned from hid_open().
     @param data The data to send, including the report number as
     the first byte.
     @param length The length in bytes of the data to send.
     @returns
     This function returns the actual number of bytes written and
     -1 on error.
     */
    int  HID_API_EXPORT HID_API_CALL hid_write(device_handle *device, const unsigned char *data, size_t length);
    
    /** @brief Read an Input report from a HID device with timeout.
     Input reports are returned
     to the host through the INTERRUPT IN endpoint. The first byte will
     contain the Report number if the device uses numbered reports.
     @ingroup API
     @param device A device handle returned from hid_open().
     @param data A buffer to put the read data into.
     @param length The number of bytes to read. For devices with
     multiple reports, make sure to read an extra byte for
     the report number.
     @param milliseconds timeout in milliseconds or -1 for blocking wait.
     @returns
     This function returns the actual number of bytes read and
     -1 on error. If no packet was available to be read within
     the timeout period, this function returns 0.
     */
    int HID_API_EXPORT HID_API_CALL hid_read_timeout(device_handle *dev, unsigned char *data, size_t length, int milliseconds);
    
    /** @brief Read an Input report from a HID device.
     Input reports are returned
     to the host through the INTERRUPT IN endpoint. The first byte will
     contain the Report number if the device uses numbered reports.
     @ingroup API
     @param device A device handle returned from hid_open().
     @param data A buffer to put the read data into.
     @param length The number of bytes to read. For devices with
     multiple reports, make sure to read an extra byte for
     the report number.
     @returns
     This function returns the actual number of bytes read and
     -1 on error. If no packet was available to be read and
     the handle is in non-blocking mode, this function returns 0.
     */
    int  HID_API_EXPORT HID_API_CALL hid_read(device_handle *device, unsigned char *data, size_t length);
    
    /** @brief Set the device handle to be non-blocking.
     In non-blocking mode calls to hid_read() will return
     immediately with a value of 0 if there is no data to be
     read. In blocking mode, hid_read() will wait (block) until
     there is data to read before returning.
     Nonblocking can be turned on and off at any time.
     @ingroup API
     @param device A device handle returned from hid_open().
     @param nonblock enable or not the nonblocking reads
     - 1 to enable nonblocking
     - 0 to disable nonblocking.
     @returns
     This function returns 0 on success and -1 on error.
     */
    int  HID_API_EXPORT HID_API_CALL hid_set_nonblocking(device_handle *device, int nonblock);
    
    /** @brief Send a Feature report to the device.
     Feature reports are sent over the Control endpoint as a
     Set_Report transfer.  The first byte of @p data[] must
     contain the Report ID. For devices which only support a
     single report, this must be set to 0x0. The remaining bytes
     contain the report data. Since the Report ID is mandatory,
     calls to hid_send_feature_report() will always contain one
     more byte than the report contains. For example, if a hid
     report is 16 bytes long, 17 bytes must be passed to
     hid_send_feature_report(): the Report ID (or 0x0, for
     devices which do not use numbered reports), followed by the
     report data (16 bytes). In this example, the length passed
     in would be 17.
     @ingroup API
     @param device A device handle returned from hid_open().
     @param data The data to send, including the report number as
     the first byte.
     @param length The length in bytes of the data to send, including
     the report number.
     @returns
     This function returns the actual number of bytes written and
     -1 on error.
     */
    int HID_API_EXPORT HID_API_CALL hid_send_feature_report(device_handle *device, const unsigned char *data, size_t length);
    
    /** @brief Get a feature report from a HID device.
     Set the first byte of @p data[] to the Report ID of the
     report to be read.  Make sure to allow space for this
     extra byte in @p data[]. Upon return, the first byte will
     still contain the Report ID, and the report data will
     start in data[1].
     @ingroup API
     @param device A device handle returned from hid_open().
     @param data A buffer to put the read data into, including
     the Report ID. Set the first byte of @p data[] to the
     Report ID of the report to be read, or set it to zero
     if your device does not use numbered reports.
     @param length The number of bytes to read, including an
     extra byte for the report ID. The buffer can be longer
     than the actual report.
     @returns
     This function returns the number of bytes read plus
     one for the report ID (which is still in the first
     byte), or -1 on error.
     */
    int HID_API_EXPORT HID_API_CALL hid_get_feature_report(device_handle *device, unsigned char *data, size_t length);
    
    /** @brief Close a HID device.
     @ingroup API
     @param device A device handle returned from hid_open().
     */
    void HID_API_EXPORT HID_API_CALL hid_close(device_handle *device);
    
    /** @brief Get The Manufacturer String from a HID device.
     @ingroup API
     @param device A device handle returned from hid_open().
     @param string A wide string buffer to put the data into.
     @param maxlen The length of the buffer in multiples of wchar_t.
     @returns
     This function returns 0 on success and -1 on error.
     */
    int HID_API_EXPORT_CALL hid_get_manufacturer_string(device_handle *device, wchar_t *string, size_t maxlen);
    
    /** @brief Get The Product String from a HID device.
     @ingroup API
     @param device A device handle returned from hid_open().
     @param string A wide string buffer to put the data into.
     @param maxlen The length of the buffer in multiples of wchar_t.
     @returns
     This function returns 0 on success and -1 on error.
     */
    int HID_API_EXPORT_CALL hid_get_product_string(device_handle *device, wchar_t *string, size_t maxlen);
    
    /** @brief Get The Serial Number String from a HID device.
     @ingroup API
     @param device A device handle returned from hid_open().
     @param string A wide string buffer to put the data into.
     @param maxlen The length of the buffer in multiples of wchar_t.
     @returns
     This function returns 0 on success and -1 on error.
     */
    int HID_API_EXPORT_CALL hid_get_serial_number_string(device_handle *device, wchar_t *string, size_t maxlen);
    
    /** @brief Get a string from a HID device, based on its string index.
     @ingroup API
     @param device A device handle returned from hid_open().
     @param string_index The index of the string to get.
     @param string A wide string buffer to put the data into.
     @param maxlen The length of the buffer in multiples of wchar_t.
     @returns
     This function returns 0 on success and -1 on error.
     */
    int HID_API_EXPORT_CALL hid_get_indexed_string(device_handle *device, int string_index, wchar_t *string, size_t maxlen);
    
    /** @brief Get a string describing the last error which occurred.
     @ingroup API
     @param device A device handle returned from hid_open().
     @returns
     This function returns a string containing the last error
     which occurred or NULL if none has occurred.
     */
    HID_API_EXPORT const wchar_t* HID_API_CALL hid_error(device_handle *device);
    
    /** @brief Get vendorId as unsigned short in base 16 format.
     @ingroup CustomHIDAPIs
     @param vId A vendorId string to convert to unsigned short.
     @returns
     This function returns unsigned short vendorId on success and 0 on error.
     */
    unsigned short convert_Vendor_Id(const char *vId);
    
    /** @brief Get productId as unsigned short in base 16 format.
     @ingroup CustomHIDAPIs
     @param pId A productId string to convert to unsigned short.
     @returns
     This function returns unsigned short productId on success and 0 on error.
     */
    unsigned short convert_Product_Id(const char *pId);
    
    
    /** @brief Get the firmware version.
     @ingroup CustomHIDAPIs
     @param handle A device handle returned from hid_open().
     @returns
     This function returns the firmware string or an empty string on error.
     */
    char* readFirmwareVersion(device_handle *handle);
    
    /** @brief Gets the UniqueId.
     @ingroup CustomHIDAPIs
     @param handle A device handle returned from hid_open().
     @returns
     This function returns the UniqueId on success and 0 on error.
     */
    unsigned int hid_get_Unique_id(device_handle *handle);
    
    /** @brief Gets the product string of the device
     @ingroup CustomHIDAPIs
     @param handle A device handle returned from hid_open().
     @returns
     This function returns the product string on success and empty string on error.
     */
    char* hid_get_ProductString(device_handle *handle);
    
    /** @brief Sets the given string as product string of the device
     @ingroup CustomHIDAPIs
     @param handle A device handle returned from hid_open().
     @param productString A string to set as product string.
     @returns
     This function returns 0 on success and -1 on error.
     */
    
    int hid_set_ProductString(device_handle *handle, const char *productString);
    
    /** @brief Sets the authentication code.
     @ingroup CustomHIDAPIs
     @param handle A device handle returned from hid_open().
     @returns
     This function returns 0 on success and -1 on error.
     */
     
    int hid_set_Authentication(device_handle *handle); 
    
    /** @brief Sets the de-authentication code.
     @ingroup CustomHIDAPIs
     @param handle A device handle returned from hid_open().
     @returns
     This function returns 0 on success and -1 on error.
     */
    
    int hid_set_Deauthentication(device_handle *handle); 
    
    /** @brief Enables or Disables the Athentication mode.
     @ingroup CustomHIDAPIs
     @param handle A device handle returned from hid_open().
     @param value 0x01 - Enable, 0x00 - Disable.
     @returns
     This function returns 0 on success and -1 on error.
     */
    
    int hid_set_Authentication_EnableOrDisable(device_handle *handle, UInt8 value); 
    
    
    /** @brief Gets the Authentication mode status.
     @ingroup CustomHIDAPIs
     @param handle A device handle returned from hid_open().
     @returns
     This function returns 
     1 - if enabled
     0 - if disabled on success and -1 on error.
     */
    
    int hid_get_Authentication_EnableOrDisable(device_handle *handle); 
    
    
    /** @brief Gets the current special effect mode.
     @ingroup CustomHIDAPIs
     @param handle A device handle returned from hid_open().
     @returns
     This function returns the mode on success and -1 on error.
     */
    int hid_get_Special_effects(device_handle *handle);
    
    /** @brief Sets the given special effect mode.
     @ingroup CustomHIDAPIs
     @param handle A device handle returned from hid_open().
     @param mode Special effect mode to set
     @returns
     This function returns 0 on success and -1 on error.
     */
    int hid_set_Special_effects(device_handle *handle, uint8_t mode);
    
    
    /** @brief Gets the denoise control value.
     @ingroup CustomHIDAPIs
     @param handle A device handle returned from hid_open().
     @returns
     This function returns the denoise control value on success and -1 on error.
     */
    int hid_get_denoise_value(device_handle *handle);
    
    /** @brief Sets the given denoise control value.
     @ingroup CustomHIDAPIs
     @param handle A device handle returned from hid_open().
     @param denoiseValue Denoise value to set.
     @returns
     This function returns 0 on success and -1 on error.
     */
    int hid_set_denoise_value(device_handle *handle, uint8_t denoiseValue);
    
    /** @brief Gets the flip mode status.
     @ingroup CustomHIDAPIs
     @param handle A device handle returned from hid_open().
     @returns
     This function returns the flip mode status on success and -1 on error.
     */
    int hid_get_flip_mode(device_handle *handle);
    
    /** @brief Sets the given flip mode.
     @ingroup CustomHIDAPIs
     @param handle A device handle returned from hid_open().
     @param mode flip mode to set.
     @returns
     This function returns 0 on success and -1 on error.
     */
    int hid_set_flip_mode(device_handle *handle, uint8_t mode); 
    
    
    /** @brief Gets the mirror mode status.
     @ingroup CustomHIDAPIs
     @param handle A device handle returned from hid_open().
     @returns
     This function returns the flip mode status on success and -1 on error.
     */
    int hid_get_mirror_mode(device_handle *handle);
    
    /** @brief Sets the given mirror mode.
     @ingroup CustomHIDAPIs
     @param handle A device handle returned from hid_open().
     @param mode mirror mode to set.
     @returns
     This function returns 0 on success and -1 on error.
     */
    int hid_set_mirror_mode(device_handle *handle, uint8_t mode); 
    
    /** @brief Gets the autoFlicker mode status.
     @ingroup CustomHIDAPIs
     @param handle A device handle returned from hid_open().
     @returns
     This function returns the autoFlicker mode status on success and -1 on error.
     */
    int hid_get_autoFlicker_mode(device_handle *handle);
    
    /** @brief Sets the given autoFlicker mode.
     @ingroup CustomHIDAPIs
     @param handle A device handle returned from hid_open().
     @param mode autoFlicker mode to set.
     @returns
     This function returns 0 on success and -1 on error.
     */
    int hid_set_autoFlicker_mode(device_handle *handle, uint8_t mode); 
        
    
    /** @brief Gets the device handle returned from hid_open().
     @ingroup CustomHIDAPIs
     @param vId VendorId string
     @param pId productId string
     @returns
     This function returns the handle to the device on success and NULL on error.
     */
    device_handle *get_device_handle(const char* vId, const char* pId);
    
    /** @brief Closes the device.
     @ingroup CustomHIDAPIs
     @param handle A device handle returned from hid_open().
     @returns
     This function returns 0 on success and -1 on error.
     */
    int hid_close_device(device_handle *handle);
  
    
#ifdef __cplusplus
}
#endif

#endif