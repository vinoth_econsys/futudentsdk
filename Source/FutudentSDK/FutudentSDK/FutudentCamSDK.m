//
//  FutudentCamSDK.m
//  FutudentSDK
//
//  Created by Appsteam on 5/1/2014.
//  Copyright © 2017 econsys. All rights reserved.
//

#import "FutudentCamSDK.h"
#import "HIDWrapper.h"
#import <AVFoundation/AVFoundation.h>

# define FUTUDENT_DEVICE_VENDOR_ID @"0x2560"
# define FUTUDENT_DEVICE_PRODUCT_ID @"0xc130"
# define trimWhiteSpaces( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ]

#ifdef DEBUG
#define DEBUG_LOG true
#else
#define DEBUG_LOG false
#endif


device_handle *deviceHandle;
deviceState *currentState;

@implementation FutudentCamSDK

@synthesize deviceDelegate;

-(id)init // constructor, initialize the necessary objects here.
{
    if(self = [super init])
    {        
        deviceHandle = NULL;       
        
        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        NSOperationQueue *mainQueue = [NSOperationQueue mainQueue];
        [notificationCenter
         addObserverForName:AVCaptureDeviceWasConnectedNotification
         object:nil
         queue:mainQueue
         usingBlock:^(NSNotification *notification)
         {
             currentState = Arrival;
             [self notificationCallBack];
         }];
        
        [notificationCenter
         addObserverForName:AVCaptureDeviceWasDisconnectedNotification
         object:nil
         queue:mainQueue
         usingBlock:^(NSNotification *notification)
         {
             currentState = Removal;
             [self notificationCallBack];
             [self releaseObjects];
         }];
        
    }
    return self;
}


-(int) Is_Device_Connected:(NSMutableArray*)deviceList
{
    // add all the Futudent cameras and return
    int isDeviceConnected = -1;
    NSArray *videoDevices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *object in videoDevices) {
        NSString *uniqueId = [object uniqueID];
        if (DEBUG_LOG) {
            NSLog(@"%@",uniqueId);
        }
        NSString *pId = [uniqueId substringFromIndex:uniqueId.length - 4];
        
        pId = [NSString stringWithFormat:@"0x%@", pId];
        if (DEBUG_LOG) {
            NSLog(@"%@",pId);
        }
        NSString *str = [uniqueId substringFromIndex:uniqueId.length - 8];
        NSString *vId = [str substringToIndex:str.length - 4];
        vId = [NSString stringWithFormat:@"0x%@", vId];
        if (DEBUG_LOG) {
            NSLog(@"%@",vId);
        }
        
        if(([pId isEqual: FUTUDENT_DEVICE_PRODUCT_ID ]) && ([vId isEqual: FUTUDENT_DEVICE_VENDOR_ID]))
        {
            isDeviceConnected = 0;
            [deviceList addObject:object.uniqueID];
            break;
        }
    }
    return isDeviceConnected;
}

-(void)notificationCallBack
{
    if (deviceDelegate != NULL)
    {
        [deviceDelegate DeviceStatusCallBackDelegate:currentState];
    }
}

-(int) Reg_DeviceStatus_CB:(device_handle*)handle CBdelegate:(id<deviceStatusProtocol>)delegate
{
    if(deviceHandle == NULL)
    {
        return ERROR_NOT_INIT;
    }
    if(handle == NULL || handle != deviceHandle)
    {
        return ERROR_INVALID_HANDLE;
    }
    if(delegate == NULL)
    {
        return FAILURE;
    }
    deviceDelegate = delegate;
    return SUCCESS;
}

-(device_handle*) initialize_Device:(NSString*)deviceId
{
    if(deviceHandle == NULL)
    {
        if(deviceId != NULL && [trimWhiteSpaces(deviceId) length] != 0)
        {
            NSString *uniqueId = deviceId;
            if (DEBUG_LOG) {
                NSLog(@"%@",uniqueId);
            }
            
            NSString *pId = [uniqueId substringFromIndex:uniqueId.length - 4];
            
            pId = [NSString stringWithFormat:@"0x%@", pId];
            if (DEBUG_LOG) {
                NSLog(@"%@",pId);
            }
            
            NSString *str = [uniqueId substringFromIndex:uniqueId.length - 8];
            NSString *vId = [str substringToIndex:str.length - 4];
            vId = [NSString stringWithFormat:@"0x%@", vId];
            if (DEBUG_LOG) {
                NSLog(@"%@",vId);
            }
            const char* vendorId = [vId cStringUsingEncoding:NSUTF8StringEncoding];
            const char* productId = [pId cStringUsingEncoding:NSUTF8StringEncoding];
            deviceHandle = get_device_handle(vendorId, productId);
            if (deviceHandle != nil) {
              int ret = hid_set_Authentication(deviceHandle);
                printf("aunthentication: %d", ret);
            }
        }
    }
    return deviceHandle;
}

-(int) Deinitalize_Device:(device_handle**)handle
{
    if(deviceHandle == NULL)
    {
        return ERROR_NOT_INIT;
    }
    if(*handle == NULL || *handle != deviceHandle)
    {
        return ERROR_INVALID_HANDLE;
    }
    int errorCode = FAILURE;
    
    errorCode = hid_set_Deauthentication(*handle);
    if (errorCode != FAILURE)
    {
        errorCode = hid_close_device(*handle);
        deviceHandle = NULL;
        currentState = NULL;
        deviceDelegate = NULL;
        *handle = NULL;
    }
    
    return errorCode;
}

-(void) releaseObjects
{    
    deviceHandle = NULL;
    currentState = NULL;
    deviceDelegate = NULL;
}

-(int) get_FirmwareVersion:(device_handle*)handle version_Info:(firmwareInfo*) versionInfo
{
    if(deviceHandle == NULL)
    {
        return ERROR_NOT_INIT;
    }
    if(handle == NULL || handle != deviceHandle)
    {
        return ERROR_INVALID_HANDLE;
    }
    if(versionInfo == NULL)
    {
        return ERROR_PARAMS_NULL;
    }
    int errorCode = FAILURE;
    
    NSString *versionNo = [NSString stringWithFormat:@"%s", readFirmwareVersion(handle)];
    if ([trimWhiteSpaces(versionNo) length ] != 0)
    {
        NSArray *versionStruct = [versionNo componentsSeparatedByString:@"."];
        int arrCount = versionStruct.count;
        int i = 0;
        // Ram - Validate versionStruct has minimum of 4 elements
        versionInfo->MajorVersion = i < arrCount ? [versionStruct[i] intValue] : 0;
        versionInfo->MinorVersion = i++ < arrCount ? [versionStruct[i] intValue] : 0;
        versionInfo->SDKVersion = i++ < arrCount ? [versionStruct[i] intValue] : 0;
        versionInfo->SVNVersion = i++ < arrCount ? [versionStruct[i] intValue] : 0;
        errorCode = SUCCESS;
    }
    
    return errorCode;
}

-(int) Get_UniqueID:(device_handle *)handle strForUniqueid:(NSString**)uniqueId
{
    if(deviceHandle == NULL)
    {
        return ERROR_NOT_INIT;
    }
    if(handle == NULL || handle != deviceHandle)
    {
        return ERROR_INVALID_HANDLE;
    }
    if(uniqueId == NULL)
    {
        return ERROR_PARAMS_NULL;
    }
    int errorCode = FAILURE;
    UInt32 result = get_Unique_id(handle);
    if (result != 0) {
        *uniqueId = [NSString stringWithFormat:@"0x%02x", result];
        errorCode = SUCCESS;
    }
    return errorCode;
    
}

-(int) Get_ProductString:(device_handle *)handle strForProductString:(NSString**)productString
{
    if(deviceHandle == NULL)
    {
        return ERROR_NOT_INIT;
    }
    if(handle == NULL || handle != deviceHandle)
    {
        return ERROR_INVALID_HANDLE;
    }
    if(productString == NULL)
    {
        return ERROR_PARAMS_NULL;
    }
    int errorCode = FAILURE;
    NSString *result = [NSString stringWithFormat:@"%s", hid_get_ProductString(handle)];
    if (![result  isEqual: @""]) {
        *productString = result;	
        errorCode = SUCCESS;
    }
    return errorCode;
    
}

-(int) Set_ProductString:(device_handle *)handle strForProductString:(NSString*)productString
{
    if(deviceHandle == NULL)
    {
        return ERROR_NOT_INIT;
    }
    if(handle == NULL || handle != deviceHandle)
    {
        return ERROR_INVALID_HANDLE;
    }
    if(productString == NULL)
    {
        return ERROR_PARAMS_NULL;
    }
    if([productString length] > 50)
    {
        return ERROR_OUTOFRANGE;
    }
    int errorCode = FAILURE;
    const char *prdString = [productString UTF8String];
    errorCode = hid_set_ProductString(handle, prdString);
    return errorCode;
    
}

-(int) set_Authentication:(device_handle*)handle {
    if(deviceHandle == NULL)
    {
        return ERROR_NOT_INIT;
    }
    if(handle == NULL || handle != deviceHandle)
    {
        return ERROR_INVALID_HANDLE;
    }
    return hid_set_Authentication(handle);    
}

-(int) set_DeAuthentication:(device_handle*)handle {
    if(deviceHandle == NULL)
    {
        return ERROR_NOT_INIT;
    }
    if(handle == NULL || handle != deviceHandle)
    {
        return ERROR_INVALID_HANDLE;
    }
    
    return hid_set_Deauthentication(handle);
    
}

-(int) get_Authentication_Status:(device_handle*)handle mode:(UInt8*)enableOrDisable{
    if(deviceHandle == NULL)
    {
        return ERROR_NOT_INIT;
    }
    if(handle == NULL || handle != deviceHandle)
    {
        return ERROR_INVALID_HANDLE;
    }
    if (enableOrDisable == NULL) {
        
        return ERROR_PARAMS_NULL;
    }
    int errorCode = FAILURE;
    errorCode = hid_get_Authentication_EnableOrDisable(handle);    
    if (errorCode != FAILURE) {
        *enableOrDisable = errorCode;
        errorCode = SUCCESS;
    }
    return errorCode;
}

-(int) set_Authentication_Status:(device_handle*)handle mode:(UInt8)enableOrDisable {
    if(deviceHandle == NULL)
    {
        return ERROR_NOT_INIT;
    }
    if(handle == NULL || handle != deviceHandle)
    {
        return ERROR_INVALID_HANDLE;
    }
    if(enableOrDisable != 0x00 && enableOrDisable != 0x01)
    {
        return ERROR_OUTOFRANGE;
    }
    return hid_set_Authentication_EnableOrDisable(handle, enableOrDisable);
    
}

-(int) get_Special_Effects:(device_handle*)handle mode:(UInt8*)effectMode {
    
    if(deviceHandle == NULL)
    {
        return ERROR_NOT_INIT;
    }
    if(handle == NULL || handle != deviceHandle)
    {
        return ERROR_INVALID_HANDLE;
    }
    if (effectMode == NULL) {
        return ERROR_PARAMS_NULL;
    }
    int errorCode = FAILURE;
    errorCode = hid_get_Special_effects(handle); 
    if (errorCode != FAILURE) {
        *effectMode = errorCode;
        errorCode = SUCCESS;
    }
    return errorCode;
    
}

-(int) set_Special_Effects:(device_handle*)handle mode:(special_Effects)effectMode {
    
    if(deviceHandle == NULL)
    {
        return ERROR_NOT_INIT;
    }
    if(handle == NULL || handle != deviceHandle)
    {
        return ERROR_INVALID_HANDLE;
    }
    return hid_set_Special_effects(handle, effectMode);
    
}

-(int) get_denoise_value:(device_handle*)handle value:(UInt8 *)denoiseValue {
    
    if(deviceHandle == NULL)
    {
        return ERROR_NOT_INIT;
    }
    if(handle == NULL || handle != deviceHandle)
    {
        return ERROR_INVALID_HANDLE;
    }
    if (denoiseValue == NULL) {
        return ERROR_PARAMS_NULL;
    }
    int errorCode = FAILURE;
    errorCode = hid_get_denoise_value(handle); 
    if (errorCode != FAILURE) {
        *denoiseValue = errorCode;
        errorCode = SUCCESS;
    }
    return errorCode;   
    
}

-(int) set_denoise_value:(device_handle*)handle value:(UInt8)denoiseValue {
    
    if(deviceHandle == NULL)
    {
        return ERROR_NOT_INIT;
    }
    if(handle == NULL || handle != deviceHandle)
    {
        return ERROR_INVALID_HANDLE;
    }
    
    if(denoiseValue < 0x00 || denoiseValue > 0x0F)
    {
        return ERROR_OUTOFRANGE;
    }
    
    return hid_set_denoise_value(handle, denoiseValue);
    
}

-(int) get_flip_mode:(device_handle*)handle  mode:(UInt8 *)flipMode{
    
    if(deviceHandle == NULL)
    {
        return ERROR_NOT_INIT;
    }
    if(handle == NULL || handle != deviceHandle)
    {
        return ERROR_INVALID_HANDLE;
    }
    if (flipMode == NULL) {
        return ERROR_PARAMS_NULL;
    }
    int errorCode = FAILURE;
    errorCode = hid_get_flip_mode(handle); 
    if (errorCode != FAILURE) {
        *flipMode = errorCode;
        errorCode = SUCCESS;
    }
    return errorCode;        
}

-(int) set_flip_mode:(device_handle*)handle mode:(UInt8)flipMode {
    
    if(deviceHandle == NULL)
    {
        return ERROR_NOT_INIT;
    }
    if(handle == NULL || handle != deviceHandle)
    {
        return ERROR_INVALID_HANDLE;
    }
    if(flipMode != 0x00 && flipMode != 0x01)
    {
        return ERROR_OUTOFRANGE;
    }
    return hid_set_flip_mode(handle, flipMode);
    
}

-(int) get_mirror_mode:(device_handle*)handle  mode:(UInt8 *)mirrorMode{
    
    if(deviceHandle == NULL)
    {
        return ERROR_NOT_INIT;
    }
    if(handle == NULL || handle != deviceHandle)
    {
        return ERROR_INVALID_HANDLE;
    }
    if (mirrorMode == NULL) {
        return ERROR_PARAMS_NULL;
    }
    int errorCode = FAILURE;
    errorCode = hid_get_mirror_mode(handle); 
    if (errorCode != FAILURE) {
        *mirrorMode = errorCode;
        errorCode = SUCCESS;
    }
    return errorCode;    
}

-(int) set_mirror_mode:(device_handle*)handle mode:(UInt8)mirrorMode {
    
    if(deviceHandle == NULL)
    {
        return ERROR_NOT_INIT;
    }
    if(handle == NULL || handle != deviceHandle)
    {
        return ERROR_INVALID_HANDLE;
    }
    if(mirrorMode != 0x00 && mirrorMode != 0x01)
    {
        return ERROR_OUTOFRANGE;
    }
    return hid_set_mirror_mode(handle, mirrorMode);
    
}

-(int) get_autoFlicker_mode:(device_handle*)handle mode:(UInt8 *)flickerMode {
    
    if(deviceHandle == NULL)
    {
        return ERROR_NOT_INIT;
    }
    if(handle == NULL || handle != deviceHandle)
    {
        return ERROR_INVALID_HANDLE;
    }
    if (flickerMode == NULL) {
        return ERROR_PARAMS_NULL;
    }
    int errorCode = FAILURE;
    errorCode = hid_get_autoFlicker_mode(handle); 
    if (errorCode != FAILURE) {
        *flickerMode = errorCode;
        errorCode = SUCCESS;
    }
    return errorCode;    
}

-(int) set_autoFlicker_mode:(device_handle*)handle mode:(UInt8)flickerMode {
    
    if(deviceHandle == NULL)
    {
        return ERROR_NOT_INIT;
    }
    if(handle == NULL || handle != deviceHandle)
    {
        return ERROR_INVALID_HANDLE;
    }
    if(flickerMode != 0x00 && flickerMode != 0x01)
    {
        return ERROR_OUTOFRANGE;
    }
    return hid_set_autoFlicker_mode(handle, flickerMode);
    
}


@end
