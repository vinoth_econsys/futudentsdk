//
//  ViewController.swift
//  FutudentApp
//
//  Created by Appsteam on 5/1/2014.
//  Copyright © 2017 econsys. All rights reserved.
//

import Cocoa
import AVFoundation
import FutudentSDK

class ViewController: NSViewController, deviceStatusProtocol {

    @IBOutlet var VideoDisplayPortion:NSImageView!
    @IBOutlet var logView:NSTextView!
    @IBOutlet var initializeBtn: NSButton!
    @IBOutlet var deInitializeBtn: NSButton!
    @IBOutlet var getAuthenticationBtn: NSButton!
    @IBOutlet var setAuthenticationBtn: NSButton!
    @IBOutlet var getSpecialEffectBtn: NSButton!
    @IBOutlet var setSpecialEffectBtn: NSButton!
    @IBOutlet var getDenoiseBtn: NSButton!
    @IBOutlet var setDenoiseBtn: NSButton!
    @IBOutlet var getFlipBtn: NSButton!
    @IBOutlet var setFlipBtn: NSButton!
    @IBOutlet var getMirrorBtn: NSButton!
    @IBOutlet var setMirrorBtn: NSButton!
    @IBOutlet var getAutoFlickBtn: NSButton!
    @IBOutlet var setAutoFlickBtn: NSButton!
    
    @IBOutlet var getProductStringBtn: NSButton!
    @IBOutlet var setProductStringBtn: NSButton!
    @IBOutlet var getUniqueIDBtn: NSButton!
    
    @IBOutlet var firmwareTextField: NSTextField!
    
    @IBOutlet var getSpecialEffectLabel: NSTextField!
    
    @IBOutlet var getDenoiseLabel: NSTextField!
    
    @IBOutlet var setDenoiseTextField: NSTextField!
    
    @IBOutlet var getFlipLabel: NSTextField!
    
    @IBOutlet var getMirrorFlipLabel: NSTextField!
    
    @IBOutlet var getAutoflickerLabel: NSTextField!
    
    @IBOutlet var getProductStringLabel: NSTextField!
    
    @IBOutlet var setProductStringTextfield: NSTextField!
    
    @IBOutlet var getUniqueIDLabel: NSTextField!
    
    @IBOutlet var setAutoFlickerTextField: NSTextField!
    
    @IBOutlet var setMirrorFlipTextField: NSTextField!
    
    @IBOutlet var setFlipTextfield: NSTextField!
    
    @IBOutlet var getFirmwareBtn: NSButton!
    @IBOutlet var specialEffectsPopUpBtn: NSPopUpButton!
    
    @IBOutlet var getAuthenticationStatusLbl: NSTextField!
    
    @IBOutlet var setAuthenticationStatusTextfield: NSTextField!
    
    var captureSession:AVCaptureSession?
    var captureDevice:AVCaptureDevice! = nil
    var previewLayer : AVCaptureVideoPreviewLayer! 
    var futudentSDKObj: FutudentCamSDK?
    var devices = NSMutableArray()
    var handle:COpaquePointer = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        futudentSDKObj = FutudentCamSDK()
        specialEffectsPopUpBtn.removeAllItems()
        resetUI(false)
        // Do any additional setup after loading the view.
    }

    func resetUI(enable:Bool)
    {
        initializeBtn.enabled = !enable
        deInitializeBtn.enabled = enable
        getAuthenticationBtn.enabled = enable
        setAuthenticationBtn.enabled = enable
        getSpecialEffectBtn.enabled = enable
        setSpecialEffectBtn.enabled = enable
        specialEffectsPopUpBtn.enabled = enable
        getDenoiseBtn.enabled = enable
        setDenoiseBtn.enabled = enable
        setDenoiseTextField.enabled = enable
        getFlipBtn.enabled = enable
        setFlipBtn.enabled = enable
        setFlipTextfield.enabled = enable
        getMirrorBtn.enabled = enable
        setMirrorBtn.enabled = enable
        setMirrorFlipTextField.enabled = enable
        getAutoFlickBtn.enabled = enable
        setAutoFlickBtn.enabled = enable
        setAutoFlickerTextField.enabled = enable
        getProductStringBtn.enabled = enable
        setProductStringBtn.enabled = enable
        setProductStringTextfield.enabled = enable
        getUniqueIDBtn.enabled = enable
        getFirmwareBtn.enabled = enable
        setAuthenticationStatusTextfield.enabled = enable
        
    }
    
    // Click events starts here
    
    
    @IBAction func initializeButton_Pressed(sender: NSButton) {
        device_initialize()
    }
    
    @IBAction func deInitializeButton_Pressed(sender: NSButton) {
        closeCamera()
        let status = futudentSDKObj?.Deinitalize_Device(&handle)
        if status == 0
        {
            resetUI(false)
        }
        logErrorMsg(("DeInitialize Device: "), code: status!)
    }
   
    @IBAction func getAuthentication_Pressed(sender: NSButton) {
        
        var authStatus:UInt8 = 0x00
        let status = futudentSDKObj?.get_Authentication_Status(handle, mode: &authStatus)
        let str = NSString.localizedStringWithFormat("0x%02x", authStatus)
        getAuthenticationStatusLbl.stringValue = str as String
        logErrorMsg(("Get Authentication status: "), code: status!)
    }
    
    @IBAction func setAuthentication_Pressed(sender: NSButton) {
        let authText = setAuthenticationStatusTextfield.stringValue.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if authText.isEmpty || authText.characters.count == 0 {
            logErrorMsg("Set Authentication: ", code: -10)
            return
        }
        let authValue = getHexValueFromString(authText) 
        
        let status = futudentSDKObj?.set_Authentication_Status(handle, mode: authValue)
        logErrorMsg(("Set Authentication: "), code: status!)
    }
    
    @IBAction func getSpecialEffect_Pressed(sender: NSButton) {
        var value:UInt8 = 0x00
        let status = futudentSDKObj?.get_Special_Effects(handle, mode: &value)
        let str = NSString.localizedStringWithFormat("0x%02x", value)
        getSpecialEffectLabel.stringValue = str as String
        logErrorMsg(("Get Special Effect: "), code: status!)            
        
    }
    
    @IBAction func setSpecialEffect_Pressed(sender: NSButton) {
        let effect = specialEffectsPopUpBtn.titleOfSelectedItem! as String
        var effectMode = NORMAL
        switch effect {
        case "Normal":
            effectMode = NORMAL
            break
        case "Black and White":
            effectMode = BLACKANDWHITE
            break
        case "Grayscale":
            effectMode = GRAYSCALE
            break
        case "Negative":
            effectMode = NEGATIVE
            break
        case "Sketch":
            effectMode = SKETCH
            break
            
        default:
            effectMode = NORMAL
            break
        }
        let status = futudentSDKObj?.set_Special_Effects(handle,mode: effectMode)
        logErrorMsg(("Set Special Effect: "), code: status!)
    }
    
    @IBAction func getDenoise_Pressed(sender: NSButton) {
        var denoisevalue:UInt8 = 0x00
        let status = futudentSDKObj?.get_denoise_value(handle,value: &denoisevalue)
        let str = NSString.localizedStringWithFormat("0x%02x", denoisevalue)
        getDenoiseLabel.stringValue = str as String
        logErrorMsg(("Get Denoise: "), code: status!)
    }
    
    @IBAction func setDenoise_Pressed(sender: NSButton) {
        let denoiseText = setDenoiseTextField.stringValue.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if denoiseText.isEmpty || denoiseText.characters.count == 0 {
            logErrorMsg("Set Denoise: ", code: -10)
            return
        }
        let denoiseValue = getHexValueFromString(denoiseText) 
        let status = futudentSDKObj?.set_denoise_value(handle, value: denoiseValue)
        logErrorMsg(("Set Denoise: "), code: status!)
    }
    
    @IBAction func getFlip_Pressed(sender: NSButton) {
        var flipMode:UInt8 = 0x00
        let status = futudentSDKObj?.get_flip_mode(handle, mode: &flipMode)
        let str = NSString.localizedStringWithFormat("0x%02x", flipMode)
        getFlipLabel.stringValue = str as String
        logErrorMsg(("Get Flip: "), code: status!)
    }
    
    @IBAction func setFlip_Pressed(sender: NSButton) {
        let flipText = setFlipTextfield.stringValue.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if flipText.isEmpty || flipText.characters.count == 0 {
            logErrorMsg("Set Flip: ", code: -10)
            return
        }
        let flipValue = getHexValueFromString(flipText) 
        let status = futudentSDKObj?.set_flip_mode(handle, mode: flipValue)
        logErrorMsg(("Set Flip: "), code: status!)
    }
    
    @IBAction func getMirror_Pressed(sender: NSButton) {
        var mirrorMode:UInt8 = 0x00
        let status = futudentSDKObj?.get_mirror_mode(handle, mode: &mirrorMode)
        let str = NSString.localizedStringWithFormat("0x%02x", mirrorMode)
        getMirrorFlipLabel.stringValue = str as String
        logErrorMsg(("Get Mirror: "), code: status!)
    }
    
    @IBAction func setMirror_Pressed(sender: NSButton) {
        let mirrorText = setMirrorFlipTextField.stringValue.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if mirrorText.isEmpty || mirrorText.characters.count == 0 {
            logErrorMsg("Set Mirror: ", code: -10)
            return
        }
        let mirrorValue = getHexValueFromString(mirrorText) 
        let status = futudentSDKObj?.set_mirror_mode(handle, mode: mirrorValue)
        logErrorMsg(("Set Mirror: "), code: status!)
    }
    
    @IBAction func getAutoFlicker_pressed(sender: NSButton) {
        var flickerMode:UInt8 = 0x00
        let status = futudentSDKObj?.get_autoFlicker_mode(handle, mode: &flickerMode)
        let str = NSString.localizedStringWithFormat("0x%02x", flickerMode)
        getAutoflickerLabel.stringValue = str as String
        logErrorMsg(("Get Auto Flicker: "), code: status!)
    }
    
  
    @IBAction func setAutoFlicker_pressed(sender: NSButton) {
        let autoFlickText = setAutoFlickerTextField.stringValue.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if autoFlickText.isEmpty || autoFlickText.characters.count == 0 {
            logErrorMsg("Set Auto Flicker: ", code: -10)
            return
        }
        let autoFlickValue = getHexValueFromString(autoFlickText)
        let status = futudentSDKObj?.set_autoFlicker_mode(handle, mode: autoFlickValue)
        logErrorMsg(("Set Auto Flicker: "), code: status!)
    }
    
    @IBAction func getFirmware_pressed(sender: NSButton) {
        var firmwareInfo = FW_Version_Info.init(MajorVersion: 0, MinorVersion: 0, SDKVersion: 0, SVNVersion: 0)
        let result = futudentSDKObj!.get_FirmwareVersion(handle, version_Info: &firmwareInfo)
        let firmware = "\(firmwareInfo.MajorVersion)." + "\(firmwareInfo.MinorVersion)." + "\(firmwareInfo.SDKVersion)." + "\(firmwareInfo.SVNVersion)"
        firmwareTextField.stringValue = firmware
        // update log
        logErrorMsg("\nGet firmware: ", code: result)
    }
    
    @IBAction func getUniqueId_pressed(sender: NSButton) {
        var uniqueIdString:NSString? = ""
        let result = futudentSDKObj!.Get_UniqueID(handle, strForUniqueid: &uniqueIdString)
        getUniqueIDLabel.stringValue = uniqueIdString as! String
        // update log
        logErrorMsg("\nGet uniqueID: ", code: result)
    }
    
    @IBAction func getProductString_Pressed(sender: NSButton) {
        var uniqueIdString:NSString? = ""
        let result = futudentSDKObj!.Get_ProductString(handle, strForProductString: &uniqueIdString)
        getProductStringLabel.stringValue = uniqueIdString as! String
        // update log
        logErrorMsg("\nGet Product string: ", code: result)
    }
    
    @IBAction func setProductString_pressed(sender: NSButton) {
        let productStringText = setProductStringTextfield.stringValue.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if productStringText.isEmpty || productStringText.characters.count == 0 {
            logErrorMsg("Set product string: ", code: -10)
            return
        }
       
        let status = futudentSDKObj?.Set_ProductString(handle, strForProductString: productStringText)
        logErrorMsg(("Set product string: "), code: status!)
    }
    
    
    @IBAction func clearLog_pressed(sender: NSButton) {
        dispatch_async(dispatch_get_main_queue(), {
            self.logView.textStorage?.setAttributedString(NSAttributedString(string: ""))
        })
    }
    
    // Device status call back, display the status next to getDevice status button in UI
    func DeviceStatusCallBackDelegate(devicestate: deviceState) {
        setDeviceStateMsg(devicestate)
    }
    
    func setDeviceStateMsg(state:deviceState)
    {
        var msg = ""
        switch state {
        case Arrival:
            msg = "Device connected "
            break
        case Removal:
            msg = "Device disconnected " 
            closeCamera()
            resetUI(false)
            break
        case NoState:
            msg = "No state "
            break
        default: 
            msg = "Unknown state "
            break
        }
        logErrorMsg(msg, code: -99)
    }
    
    func device_initialize() {
        
        let isconnected = futudentSDKObj!.Is_Device_Connected(devices)
        print(isconnected)
        
        if devices.count > 0 
        {            
            handle = futudentSDKObj!.initialize_Device(devices[0] as! String)
            if handle != nil {
                resetUI(true)
            openCam()      
                // register the device status call back
                let result = futudentSDKObj!.Reg_DeviceStatus_CB(handle, CBdelegate: self)
                logErrorMsg("\nRegister Device status call back: ", code: result)
                
                specialEffectsPopUpBtn.removeAllItems()
                specialEffectsPopUpBtn.addItemWithTitle("Normal")
                specialEffectsPopUpBtn.addItemWithTitle("Black and White")
                specialEffectsPopUpBtn.addItemWithTitle("Grayscale")
                specialEffectsPopUpBtn.addItemWithTitle("Negative")
                specialEffectsPopUpBtn.addItemWithTitle("Sketch")
                
            }            
            logErrorMsg("\nDevice Initialize: ", code: handle != nil ? 0 : -1)    
            
        }
        else{
            logErrorMsg("\nGet Devices: ", code: -81)
        }
    }
    override var representedObject: AnyObject? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    func openCam()
    {
        captureSession = AVCaptureSession()
        
        
        let videoDevices = AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo)
        
        for device in videoDevices{
            let device = device as! AVCaptureDevice
            if device.localizedName.containsString("Futudent") {
                captureDevice = device                
                break
            }
        }
        
        if captureDevice == nil {
            print("Cannot find futudent camera device!!")
            logErrorMsg("Cannot find futudent camera device!!", code: 0)
            return
        }
        do {
                        
            let input = try? AVCaptureDeviceInput(device: captureDevice)
            
            if (captureSession!.canAddInput(input)){
                
                captureSession!.addInput(input)            
                VideoDisplayPortion.wantsLayer = true
                VideoDisplayPortion.layer = CALayer()
                VideoDisplayPortion.layer?.backgroundColor = NSColor.blackColor().CGColor
                
                previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                previewLayer.videoGravity =  AVLayerVideoGravityResizeAspectFill
                VideoDisplayPortion.autoresizesSubviews = true
                previewLayer?.frame = VideoDisplayPortion.bounds
                VideoDisplayPortion.layer?.addSublayer(previewLayer)
                captureSession!.startRunning()
                captureSession!.sessionPreset = AVCaptureSessionPreset640x480            
            }
        }
    }
    
    func closeCamera()
    {
        if captureSession != nil {
            captureSession?.stopRunning()
        }
        captureSession = nil
        captureDevice = nil
    }

    func logErrorMsg(functionName:String, code:Int32)
    {
        var errorMsg = ""
        switch code {
        case 0:
            errorMsg = "Success"
            break
        case -1:
            errorMsg = "failed"
            break
        case -2:
            errorMsg = "Invalid device handle"
            break
        case -3:
            errorMsg = "Values out of range"
            break
        case -4:
            errorMsg = "parameter is nil"
            break
        case -5:
            errorMsg = "Device not initialized"
            break
        case -10:
            errorMsg = "Invalid Parameters"
            break
        case -81:
            errorMsg = "No Device found"
            break
        case -99:
            errorMsg = ""
            break
        default:
            errorMsg = "unknown error"
            break
        }
        
        dispatch_async(dispatch_get_main_queue(), {
            self.logView.textStorage?.appendAttributedString(NSAttributedString(string: "\n\(functionName)" + errorMsg))
            
            if (self.logView.textContainer?.containerSize.height > self.logView.frame.size.height)
            {
                let range = NSMakeRange((self.logView.string! as NSString).length - 1, 1);
                self.logView.scrollRangeToVisible(range)
            }
        })
        
    }

    func getHexValueFromString(string:String) -> UInt8
    {
        var numbers = [UInt8]()
        var from = string.startIndex
        while from != string.endIndex {
            let to = from.advancedBy(2, limit: string.endIndex)
            numbers.append(UInt8(string[from ..< to], radix: 16) ?? 0)
            from = to
        }
        return numbers[0]
    }
}

